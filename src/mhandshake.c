// mhandshake is a WireGuard handshake proxy that allows
// endpoint discovery within the same network, without
// a central server. 
// 
// Disclamer: 
// This proof of concept lacks review and testing. It may be insecure. It should not be used in production.  
// Please refer to Disclaimer of Warranty within the LICENSE file.    
// 
// Usage:
// * Set peers endpoint to localhost:8080, 
// * Run mhandshake on peers with the same arguments. 
// * After successful handshake forwarning, the endpoint is automatically updated the real peer's host:port 
//
// How it works: 
// * Outgoing handshakes are received on localhost:8080, 
//   and forwarded to the selected multicast group. 
// * Incoming handshakes received on the multicast group
//   are forwarded to the local wireguard port. 
// * If WireGuard recognizes a valid handshake and reply,
//   mhandshake forwards the reply via unicast to the peer
//   real's endpoint. 
// * If the peer accepts the response, the handshake is 
//   completed, and the peer sets the endpoint with
//   our unicast address. 

#include <sys/poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/in.h>

#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// For context, read WireGuard's protocol overview at 
// https://www.wireguard.com/protocol/

typedef enum wg_msg_type {
    wg_msg_invalid = 0,
    wg_msg_hinit   = 1, // size == 148
    wg_msg_hresp   = 2, // size == 92
    wg_msg_cookie  = 3, // size == 64
    wg_msg_data    = 4  // size >= 16
} wg_msg_type_t;

static wg_msg_type_t parse_msg_type(const char *payload, size_t payload_size)
{
    if (payload_size < 16)
        return wg_msg_invalid;

    if (payload[1] != 0x0 || payload[2] != 0x0 || payload[3] != 0x0)
        return wg_msg_invalid;

    if (payload[0] < wg_msg_hinit || payload[0] > wg_msg_data)
        return wg_msg_invalid;
    
    if (payload[0] == wg_msg_hinit && payload_size != 148)
        return wg_msg_invalid;

    if (payload[0] == wg_msg_hresp && payload_size != 92)
        return wg_msg_invalid;

    if (payload[0] == wg_msg_cookie && payload_size != 64)
        return wg_msg_invalid;

    return (wg_msg_type_t) payload[0];
}

static bool parse_send_index(const char *payload, size_t payload_size, uint32_t *send_idx)
{
    if (payload_size < 16)
        return false;

    if (payload[1] != 0x0 || payload[2] != 0x0 || payload[3] != 0x0)
        return false;

    if (payload[0] != wg_msg_hinit && payload[0] != wg_msg_hresp)
        return false;
    
    uint32_t *payload_send_idx = (uint32_t *) (&payload[4]);
    if (send_idx)
        *send_idx = *payload_send_idx;
    return true;
}

static bool parse_sendrecv_indices(const char *payload, size_t payload_size, uint32_t *send_idx, uint32_t *recv_idx)
{
    if (payload_size < 16)
        return false;

    if (payload[1] != 0x0 || payload[2] != 0x0 || payload[3] != 0x0)
        return false;

    if (payload[0] != wg_msg_hresp)
        return false;
    
    uint32_t *payload_send_idx = (uint32_t *) (&payload[4]);
    uint32_t *payload_recv_idx = (uint32_t *) (&payload[8]);
    if (send_idx)
        *send_idx = *payload_send_idx;
    if (recv_idx)
        *recv_idx = *payload_recv_idx;
    return true;
}

// Sender record. 
// Includes: IP address, UDP port, WireGuard handshake sender index. 
typedef struct wg_sender_record {
    struct sockaddr_storage send_addr;
    uint32_t send_idx;
} wg_sender_record_t;

// Sender ring buffer
typedef struct wg_sender_rbuffer {
    wg_sender_record_t *records;
    uint16_t record_capacity;
    uint16_t record_count;
    uint16_t record_last;
} wg_sender_rbuffer_t;

void sender_rbuffer_init(wg_sender_rbuffer_t *rbuffer, uint16_t capacity)
{
    rbuffer->records = calloc(capacity, sizeof(wg_sender_record_t));
    rbuffer->record_capacity = capacity;
    rbuffer->record_count = 0;
    rbuffer->record_last = 0;
}

void sender_rbuffer_add(wg_sender_rbuffer_t *rbuffer, const wg_sender_record_t* record)
{
    uint16_t record_idx = (rbuffer->record_last + 1) % rbuffer->record_capacity;
    memcpy(&(rbuffer->records[record_idx]), record, sizeof(wg_sender_record_t));
    rbuffer->record_last = record_idx;
    if (record_idx + 1 > rbuffer->record_count)
        rbuffer->record_count = record_idx + 1;
}

wg_sender_record_t *sender_rbuffer_find(wg_sender_rbuffer_t *rbuffer, uint32_t sender_idx)
{
    for (uint16_t i = 0; i < rbuffer->record_count; ++i)
    {
        wg_sender_record_t *record = &(rbuffer->records[i]);
        if (record->send_idx == sender_idx)
            return record;
    }

    return NULL;
}

void sockaddr_init(struct sockaddr_storage *addr_stor, int addr_family, const char *addr, uint16_t port)
{
    memset(addr_stor, 0, sizeof(struct sockaddr_storage));
    if (addr_family == AF_INET)
    {
        struct sockaddr_in *addr4 = (struct sockaddr_in *) addr_stor;
        addr4->sin_family = AF_INET;
        addr4->sin_addr.s_addr = inet_addr(addr);
        addr4->sin_port = htons(port);
    }
    else if (addr_family == AF_INET6)
    {
        struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *) addr_stor;
        addr6->sin6_family = AF_INET6;
	    inet_pton(AF_INET6, addr, &(addr6->sin6_addr));
	    addr6->sin6_port = htons(port);
    }
}

void sender_rbuffer_remove(wg_sender_rbuffer_t *rbuffer, wg_sender_record_t* record)
{
    memset(record, 0, sizeof(wg_sender_record_t));
}

in_port_t sockaddr_get_port(const struct sockaddr_storage *addr_stor)
{
    if (addr_stor->ss_family == AF_INET)
    {
        struct sockaddr_in *addr4 = (struct sockaddr_in *)addr_stor;
        return addr4->sin_port;
    }
    else if (addr_stor->ss_family == AF_INET6)
    {
        struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *)addr_stor;
        return addr6->sin6_port;
    }
    else
    {
        fputs("sockaddr_get_port: unknown internet protocol\n", stderr);
        return 0;
    }
}

void sockaddr_set_port(struct sockaddr_storage *addr_stor, in_port_t port)
{
    if (addr_stor->ss_family == AF_INET)
    {
        struct sockaddr_in *addr4 = (struct sockaddr_in *)addr_stor;
        addr4->sin_port = port;
    }
    else if (addr_stor->ss_family == AF_INET6)
    {
        struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *)addr_stor;
        addr6->sin6_port = port;
    }
    else
    {
        fputs("sockaddr_set_port: unknown internet protocol\n", stderr);
    }
}

void wg_forward(int fd, char *buf, size_t buf_len, const struct sockaddr_storage *from_addr, const struct sockaddr_storage *to_addr)
{
    if ( bind(fd, (const struct sockaddr *) from_addr, sizeof(struct sockaddr_storage)) < 0 )
    {
        perror("wg_forward/bind");
        return;
    }

    sendto(fd, buf, buf_len, 0, (const struct sockaddr *) to_addr, sizeof(struct sockaddr_storage));
}

void wg_forward_raw(int fd, char *buf, size_t buf_len, const struct sockaddr_storage *from_addr, const struct sockaddr_storage *to_addr)
{
    // 256 > 8 (udp header size) 148 (wg hinit payload)
    char raw_buf[256];
    if (buf_len > sizeof(raw_buf))
    {
        perror("wg_forward/buf_len");
        return;
    }

    in_port_t udp_srcport_ns = sockaddr_get_port(from_addr);
    in_port_t udp_dstport_ns = sockaddr_get_port(to_addr);
    uint16_t udp_len = 8 + buf_len;
    uint16_t udp_len_ns = htons(udp_len);
    uint16_t udp_checksum_ns = 0;

    memcpy(raw_buf, &udp_srcport_ns, sizeof(udp_srcport_ns));
    memcpy(raw_buf + 2, &udp_dstport_ns, sizeof(udp_dstport_ns));
    memcpy(raw_buf + 4, &udp_len_ns, sizeof(udp_len_ns));
    memcpy(raw_buf + 6, &udp_checksum_ns, sizeof(udp_checksum_ns));
    memcpy(raw_buf + 8, buf, buf_len);

    struct sockaddr_storage raw_to_addr = *to_addr;
    sockaddr_set_port(&raw_to_addr, 1234);
    if(sendto(fd, raw_buf, udp_len, 0, (const struct sockaddr *) &raw_to_addr, sizeof(raw_to_addr)) < 0)
    {
        perror("wg_forward_raw/sendto");
    }
}

const char *address_to_str(const struct sockaddr_storage *addr_stor, char *buf, size_t buf_len)
{
    if (addr_stor->ss_family == AF_INET)
    {
        struct sockaddr_in *addr4 = (struct sockaddr_in *)addr_stor;
        const char *str = inet_ntop(AF_INET, &(addr4->sin_addr), buf, buf_len);
        if (str)
        {
            size_t used_len = strlen(str);
            char *buf_port = &(buf[used_len]);
            snprintf(buf_port, buf_len-used_len, ":%hu", ntohs(addr4->sin_port));
        }
        return str;
    }
    else if (addr_stor->ss_family == AF_INET6)
    {
        struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *)addr_stor;
        const char *str = inet_ntop(AF_INET6, &(addr6->sin6_addr), buf, buf_len);
        if (str)
        {
            size_t used_len = strlen(str);
            char *buf_port = &(buf[used_len]);
            snprintf(buf_port, buf_len-used_len, ":%hu", ntohs(addr6->sin6_port));
        }
        return str;
    }
    else
        return "Unknown protocol family";
}

// received from localhost:
// hinit -> record srcport+wg_sender, fw to multicast group
// hresp|cookie -> lookup wg_receiver against recorder wg_sender, then delete records, then fw from recorder port to recorded ip+port
// ...   -> drop

// received from multicast group:
// hinit -> record ip+port+wg_sender, then fw to localhost
// ...   -> drop

int main(int argc, char *argv[])
{
    if (argc != 4) {
       printf("Usage: \n  %s mcast_address mcast_port local_wg_port\n\n", argv[0]);
       printf("Example for RFC3692-style experiment: \n  %s 224.0.0.254 37555 51820\n\n", argv[0]);
       return 1;
    }

    char* mcast_addr_arg = argv[1];
    uint16_t mcast_port = atoi(argv[2]); // 0 if error, which is an invalid port
    uint16_t local_wg_port = atoi(argv[3]);
    const char *local_addr_arg = "127.0.0.1"; // XXX arg
    uint16_t local_port = 8080; // XXX arg

    // Socket for local UDP communication. 
    int local_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (local_fd < 0) {
        perror("socket");
        return EXIT_FAILURE;
    }

    int reusable = 1;
    if (setsockopt(local_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&reusable, sizeof(reusable)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }

    // Local address for UDP proxy
    struct sockaddr_in local_addr;
    memset(&local_addr, 0, sizeof(local_addr));
    local_addr.sin_family    = AF_INET; // IPv4
    local_addr.sin_addr.s_addr = inet_addr(local_addr_arg);
    local_addr.sin_port = htons(local_port);

    // Address for Wireguard local peer
    struct sockaddr_storage local_wg_addr;
    sockaddr_init(&local_wg_addr, AF_INET, "127.0.0.1", local_wg_port);

    if ( bind(local_fd, (const struct sockaddr *)&local_addr, 
            sizeof(local_addr)) < 0 )
    {
        perror("bind");
        return EXIT_FAILURE;
    }

    // Socket for unicast UDP forwarding. 
    // XXX cleanup is this socket even necessary? 
    int fw_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fw_fd < 0) {
        perror("socket");
        return EXIT_FAILURE;
    }

    if (setsockopt(fw_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&reusable, sizeof(reusable)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }

    int raw_fd = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
    if (raw_fd < 0) {
        perror("socket(SOCK_RAW)");
        return EXIT_FAILURE;
    }

    if (setsockopt(raw_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&reusable, sizeof(reusable)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }

    int mcast_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (mcast_fd < 0) {
        perror("socket(SOCK_DGRAM)");
        return EXIT_FAILURE;
    }

    int mcast_recv_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (mcast_recv_fd < 0) {
        perror("socket(SOCK_DGRAM)");
        return EXIT_FAILURE;
    }

    struct sockaddr_storage mcast_recv_addr;
    sockaddr_init(&mcast_recv_addr, AF_INET, "0.0.0.0", mcast_port);

    if (bind(mcast_recv_fd, (const struct sockaddr *) &mcast_recv_addr, sizeof(mcast_recv_addr)) < 0)
    {
        perror("wg_forward/bind");
        return EXIT_FAILURE;
    }

    struct sockaddr_storage mcast_group_addr;
    sockaddr_init(&mcast_group_addr, AF_INET, mcast_addr_arg, mcast_port);

    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr = inet_addr(mcast_addr_arg);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(raw_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }
    if (setsockopt(mcast_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }
    if (setsockopt(mcast_recv_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
    {
        perror("setsockopt");
        return EXIT_FAILURE;
    }

    struct sockaddr_storage mcast_src_addr;
    sockaddr_init(&mcast_src_addr, AF_INET, "0.0.0.0", mcast_port);

    struct pollfd poll_fds[2];
    poll_fds[0].fd = local_fd;
    poll_fds[0].events = POLLIN;
    poll_fds[1].fd = mcast_recv_fd;
    poll_fds[1].events = POLLIN;

    wg_sender_rbuffer_t local_sender_records, remote_sender_records;
    sender_rbuffer_init(&local_sender_records, 16);
    sender_rbuffer_init(&remote_sender_records, 64);

    // Buffer for temporary packet storage
    char wg_buffer[256];

    // Record for temporary local address and index storage
    wg_sender_record_t local_sendrec;
    struct sockaddr *local_sendrec_addr = (struct sockaddr *) &(local_sendrec.send_addr);

    // Record for temporary remote address and index storage
    wg_sender_record_t remote_sendrec; 
    struct sockaddr *remote_sendrec_addr = (struct sockaddr *) &(remote_sendrec.send_addr);

    while (1)
    {
        if (poll(poll_fds, 2, -1) < 0)
        {
            perror("poll");
            return EXIT_FAILURE;
        }

        if (poll_fds[0].revents & POLLIN)
        {
            socklen_t from_len = sizeof(local_sendrec.send_addr);
            ssize_t read_bytes = recvfrom(
                local_fd, wg_buffer, sizeof(wg_buffer), 0, local_sendrec_addr, &from_len);

            char buf[128];
            printf("Received %ld bytes on port %hu from %s\n",
                read_bytes, local_port, address_to_str(&(local_sendrec.send_addr), buf, sizeof(buf)));

            if (read_bytes == -1)
            {
                perror("recvfrom");
            }
            else if (read_bytes > 0)
            {
                wg_msg_type_t msg_type = parse_msg_type(wg_buffer, read_bytes);
                printf("message type: %d\n", msg_type);

                if (msg_type == wg_msg_hinit
                    && parse_send_index(wg_buffer, read_bytes, &(local_sendrec.send_idx)))
                {
                    sender_rbuffer_add(&local_sender_records, &local_sendrec);
                    printf("Added local record zith index 0x%08X\n", local_sendrec.send_idx);

                    sockaddr_set_port(&mcast_src_addr, sockaddr_get_port(&(local_sendrec.send_addr)));
                    //wg_forward(mcast_fd, wg_buffer, read_bytes, &mcast_src_addr, &mcast_group_addr);
                    wg_forward_raw(raw_fd, wg_buffer, read_bytes, &mcast_src_addr, &mcast_group_addr);
                    //sendto(mcast_fd, wg_buffer, read_bytes, 0, (struct sockaddr *)&mcast_group_addr, sizeof(mcast_group_addr));
                }
                else if (msg_type == wg_msg_hresp
                    && parse_sendrecv_indices(wg_buffer, read_bytes, &(local_sendrec.send_idx), &(remote_sendrec.send_idx)))
                {
                    //wg_sender_record_t *local_record = sender_rbuffer_find(&local_sender_records, local_sendrec.send_idx);
                    wg_sender_record_t *remote_record = sender_rbuffer_find(&remote_sender_records, remote_sendrec.send_idx);
                    //printf("local sender index 0x%08X; record: %s\n", local_sendrec.send_idx, local_record ? "found" : "not found");
                    printf("remote sender index 0x%08X; record: %s\n", remote_sendrec.send_idx, remote_record ? "found" : "not found");
                    //if (local_record && remote_record)
                    if (remote_record)
                    {
                        //wg_forward(fw_fd, wg_buffer, read_bytes, &(local_record->send_addr), &(remote_record->send_addr));
                        wg_forward_raw(raw_fd, wg_buffer, read_bytes, local_sendrec_addr, &(remote_record->send_addr));
                        //sender_rbuffer_remove(&local_sender_records, local_record);
                        sender_rbuffer_remove(&remote_sender_records, remote_record);
                    }

                }
            }
        }

        if (poll_fds[1].revents & POLLIN)
        {
                            char buf[128];

            socklen_t from_len = sizeof(remote_sendrec.send_addr);
            ssize_t read_bytes = recvfrom(
                mcast_recv_fd, wg_buffer, sizeof(wg_buffer), 0, remote_sendrec_addr, &from_len);
            //int read_bytes = read(ucast_fd, wg_buffer, sizeof(wg_buffer));
            printf("Received %ld bytes on port %hu from %s\n", read_bytes, local_port, address_to_str(&(remote_sendrec.send_addr), buf, sizeof(buf)));
            if (read_bytes > 0)
            {
                wg_msg_type_t msg_type = parse_msg_type(wg_buffer, read_bytes);
                printf("Message type: %d\n", msg_type);
                if (msg_type == wg_msg_hinit
                    && parse_send_index(wg_buffer, read_bytes, &(remote_sendrec.send_idx)))
                {
                    sender_rbuffer_add(&remote_sender_records, &remote_sendrec);
                    printf("Added remote record with index 0x%08X\n", remote_sendrec.send_idx);

                    wg_forward_raw(raw_fd, wg_buffer, read_bytes, &local_addr, &local_wg_addr);
                }
            }
        }
    }

    close(local_fd);
    close(fw_fd);
    close(mcast_fd);
    close(mcast_recv_fd);

    return 0;
}
